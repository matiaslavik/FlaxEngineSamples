﻿#if FLAX_EDITOR
using FlaxEditor;
using FlaxEditor.CustomEditors;
using FlaxEngine;
using FlaxEngine.GUI;
using System.Collections.Generic;

namespace Game
{
    public class VisualNode
    {
        private ContainerControl nodeRoot;

        public VisualNode(ContainerControl parent, string nodeName, string[] inputStrings, string outputString)
        {
            // Root node
            nodeRoot = parent.AddChild<VerticalPanel>();
            nodeRoot.Width = 200;
            nodeRoot.BackgroundColor = Color.Gray;

            // Title label (function name, etc.)
            var label = nodeRoot.AddChild<Label>();
            label.Text = nodeName;
            label.Width = nodeRoot.Width;
            label.BackgroundColor = Color.Blue;

            var content = nodeRoot.AddChild<HorizontalPanel>();
            var inputs = content.AddChild<VerticalPanel>();
            inputs.AnchorPreset = AnchorPresets.MiddleLeft;

            // Inputs
            foreach (string inputString in inputStrings)
            {
                var input = inputs.AddChild<Label>();
                input.Text = inputString;
                input.VerticalAlignment = TextAlignment.Near;
            }

            // Ouotputs
            var outputs = content.AddChild<VerticalPanel>();
            outputs.AnchorPreset = AnchorPresets.MiddleRight;

            var output = outputs.AddChild<Label>();
            output.Text = outputString;
            output.VerticalAlignment = TextAlignment.Near;
        }

        public ContainerControl GetControl()
        {
            return nodeRoot;
        }
    }

    public class VisualScriptingEditorWindow : CustomEditorWindow
    {
        private List<VisualNode> nodes;
        private ContainerControl nodeRoot;
        private VisualNode selectedNode = null;
        private Vector2 mousePosRelToNode;

        public override void Initialize(LayoutElementsContainer layout)
        {
            nodeRoot = layout.ContainerControl.AddChild<Panel>();
            nodeRoot.Width = 800;
            nodeRoot.Height = 600;

            nodes = new List<VisualNode>();

            nodes.Add(new VisualNode(nodeRoot, "updatePlayer()", new string[] { "Health", "Level" }, "Result"));
            nodes.Add(new VisualNode(nodeRoot, "loadMap()", new string[] { "MapName" }, "Result"));
            
            for (int i = 0; i < nodes.Count; ++i)
            {
                nodes[1].GetControl().Location = new Vector2(250.0f * i, 0.0f);
            }
        }

        public override void Refresh()
        {
            base.Refresh();

            Vector2 mousePos = nodeRoot.PointFromScreen(Input.Mouse.Position);

            // MouseDown => Select new node
            if (selectedNode == null && Input.GetMouseButtonDown(MouseButton.Left))
            {
                foreach (VisualNode node in nodes)
                {
                    if (node.GetControl().GetClientArea().Contains(node.GetControl().PointFromParent(mousePos)))
                    {
                        selectedNode = node;
                        mousePosRelToNode = mousePos - node.GetControl().Location;
                        break;
                    }
                }
            }

            // MouseHold => Move selected node
            if (selectedNode != null && Input.GetMouseButton(MouseButton.Left))
            {
                selectedNode.GetControl().Location = mousePos - selectedNode.GetControl().Size / 2;
            }

            // MouseUp => Clear selected node
            if (selectedNode != null && Input.GetMouseButtonUp(MouseButton.Left))
            {
                selectedNode = null;
            }
        }
    }
}
#endif
