﻿#if FLAX_EDITOR
using FlaxEditor;
using FlaxEditor.GUI;

namespace Game
{
    public class VisualScriptingEditorPlugin : EditorPlugin
    {
        private ToolStripButton _button;

        public override void InitializeEditor()
        {
            base.InitializeEditor();

            _button = Editor.UI.ToolStrip.AddButton("Open visual scripting editor");
            _button.Clicked += () => new VisualScriptingEditorWindow().Show();
        }

        public override void Deinitialize()
        {
            if (_button != null)
            {
                _button.Dispose();
                _button = null;
            }

            base.Deinitialize();
        }
    }
}
#endif
