# FlaxEngine Samples Projects

Some sample projects for FlaxEngine.

## Contents

### MeshModificationSample

Shows how to modify the mesh of a StaticModel at runtime. See the code in MeshModificationTest.cs.

This is done using the `Mesh.UpdateMesh` function, which only works on Virtual assets. We therefore need to first call `Content.CreateVirtualAsset<Model>()` to create a new virtual model asset.

**How to use:**
1. Open TestScene.scene.
2. Click play.
3. You will se a duck where the mesh gradually gets distorted/exploded.

<img src="RepoMedia/MeshModificationSample.jpg" width="300px">

### VisualScriptingEditorSample

Shows how you could create a visual scripting editor or any other editor window that requires movable GUI elements.
To do this, we use `FlaxEngine.GUI` rather than `LayoutElementsContainer` to allow us to manually position the GUI elements.
AFAIK there is no way to draw lines between the nodes, so for that you would probably have to do some manual rendering to a texture.

**How to use:**
1. Open TestScene.scene.
2. Click on the "open visual scripting editor" button in the upper toolbar, just to the right of the play button.
3. In the editor window that opens: Move the nodes around.

<img src="RepoMedia/VisualScriptingEditorSample.jpg" width="300px">

### RaycastSample

Example project showing how do a raycast from the mouse position through the scene, and adding an impulse to the hit result's rigidbody.

**How to use:**
1. Open TestScene.scene.
2. Click play.
3. Click on the box => It will be kicked away.

<img src="RepoMedia/RaycastSample.jpg" width="300px">
