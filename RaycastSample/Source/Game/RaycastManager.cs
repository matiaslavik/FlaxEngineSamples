﻿using System;
using System.Collections.Generic;
using FlaxEngine;

namespace Game
{
    public class RaycastManager : Script
    {
        public override void OnUpdate()
        {
            // On mouse click => Raycast from mouse, and add impulse to hit object.
            if (Input.GetMouseButtonDown(MouseButton.Left))
            {
                Ray ray = Camera.MainCamera.ConvertMouseToRay(Input.MousePosition);

                RayCastHit hit;
                if (Physics.RayCast(ray.Position, ray.Direction, out hit))
                {
                    if (hit.Collider.AttachedRigidBody != null)
                        hit.Collider.AttachedRigidBody.AddForceAtPosition(ray.Direction * 10000, ray.Position, ForceMode.Impulse);
                }
            }
        }
    }
}
