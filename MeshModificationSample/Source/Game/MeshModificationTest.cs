﻿using System;
using System.Collections.Generic;
using FlaxEngine;

namespace Game
{
    public class MeshModificationTest : Script
    {
        private Model model = null;
        private Mesh.Vertex[] verts = null;
        private uint[] indices = null;

        private Vector3[] vertPositions;
        private Vector3[] vertNormals;
        private Vector2[] vertUVs;

        public override void OnStart()
        {
            // Get original mesh
            StaticModel staticModel = (StaticModel)Actor;
            Debug.Assert(staticModel != null, "This Script requires a StaticModel.");
            Mesh mesh = staticModel.Model.LODs[0].Meshes[0];
            ModelInstanceEntry[] entries = staticModel.Entries;

            // Fetch vertices and indices
            verts = mesh.DownloadVertexBuffer();
            indices = mesh.DownloadIndexBuffer();

            // Create new virtual model (only virtual model can be modified) and setup LOD 0 mesh
            model = Content.CreateVirtualAsset<Model>();
            model.SetupLODs(new int[1] { 1 });

            // Copy vertex data to array of Vector3
            vertPositions = new Vector3[verts.Length];
            vertNormals = new Vector3[verts.Length];
            vertUVs = new Vector2[verts.Length];
            for (int iVert = 0; iVert < verts.Length; iVert++)
            {
                vertPositions[iVert] = verts[iVert].Position;
                vertNormals[iVert] = verts[iVert].Normal;
                vertUVs[iVert] = verts[iVert].TexCoord;
            }
            
            model.LODs[0].Meshes[0].MaterialSlotIndex = staticModel.Model.LODs[0].Meshes[0].MaterialSlotIndex;

            // Copy vertices and indices to new mesh
            model.LODs[0].Meshes[0].UpdateMesh(vertPositions, indices, vertNormals, null, vertUVs);
            
            // Assign new virtual model to StaticModel
            staticModel.Model = model;

            // Assigning Model will break the existing entries, for some reason
            staticModel.Entries = entries;
        }

        public override void OnUpdate()
        {
            if (Time.GameTime > 1.0f) // wait a second
            {
                Random rand = new Random();

                Mesh mesh = model.LODs[0].Meshes[0];
                for (int iVert = 0; iVert < verts.Length; iVert++)
                {
                    // Shoot all vertices in "random" direction
                    Vector3 vertOffsetDir = new Vector3((float)rand.NextDouble(), (float)rand.NextDouble(), (float)rand.NextDouble()) * verts[iVert].Position.Normalized;
                    vertPositions[iVert] = verts[iVert].Position + iVert * vertOffsetDir * 0.001f * Time.DeltaTime;
                    verts[iVert].Position = vertPositions[iVert];
                }
                mesh.UpdateMesh(vertPositions, indices);
            }
        }
    }
}
